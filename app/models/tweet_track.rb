class TweetTrack < ApplicationRecord

	def self.update_track(tweet_id , twitter_account_id)
		tweet_track = TweetTrack.where(tweet_id: tweet_id, twitter_account_id: twitter_account_id)
		if tweet_track != []
			times = tweet_track.last.times + 1
			tweet_track.last.update(times: times)
		else
			TweetTrack.create(tweet_id: tweet_id, twitter_account_id: twitter_account_id, times: 1)
		end
	end
end
