class TwitterAccount < ApplicationRecord

	FORBIDDEN_WORDS = ["culo", "caca", "tetas"]

	def fav( tweet_id = 0 )
		return if tweet_id == 0 
		client.fav!(tweet_id)
	end

    def write( tweet_id = 0 )
		return if tweet_id == 0 
		client.update(Tweet.find(tweet_id).text)
		TweetTrack.update_track(tweet_id, id)
	end

	private

	def client
		@client ||= Twitter::REST::Client.new do |config|
		  config.consumer_key        = consumer_key
		  config.consumer_secret     = consumer_secret
		  config.access_token        = access_token
		  config.access_token_secret = access_token_secret
		end
	end

	def write_direct_tweet(pass = 0, text = '')
		return if pass != 1976 || text.include?(SELF::FORBIDDEN_WORDS)
		client.update(text)
	end

end
