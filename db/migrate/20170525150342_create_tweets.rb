class CreateTweets < ActiveRecord::Migration[5.1]
  def change
    create_table :tweets do |t|
      t.string :text
      t.integer :type
      t.boolean :has_link
      t.boolean :has_hastag

      t.timestamps
    end
  end
end
