class CreateTweetTracks < ActiveRecord::Migration[5.1]
  def change
    create_table :tweet_tracks do |t|
      t.integer :times
      t.integer :tweet_id
      t.integer :twitter_account_id

      t.timestamps
    end
  end
end
